import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'assignment';
  HomeText="";
  aboutText="";
  servicesText="";
  contactText="";


  constructor() { }

  ngOnInit() {
  }

  sayHome(event : any) {
    this.HomeText = event;
    this.contactText="";
    this.aboutText="";
    this.servicesText="";
    
  }

  sayAbout(event : any) {
    this.aboutText = event;
    this.HomeText="";
    this.contactText="";
    this.servicesText="";
  }

  sayService(event : any) {
    this.servicesText = event;
    this.HomeText="";
    this.aboutText="";
    this.contactText="";
   
  }

  sayContact(event : any) {
    this.contactText = event;
    this.HomeText="";
    this.aboutText="";
    this.servicesText="";
  }
}

