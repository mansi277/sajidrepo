import { Component, EventEmitter, OnInit,Output } from '@angular/core';


@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Output() about: EventEmitter<string> = new EventEmitter<string>();
  @Output() services: EventEmitter<string> = new EventEmitter<string>();
  @Output() client: EventEmitter<string> = new EventEmitter<string>();
  @Output() contact: EventEmitter<string> = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }

  sayHome($event : any) {
    this.about.emit('Hello Home button , You have clicked the home button ');
  }

  sayAbout($event : any) {
    this.services.emit('Hello About button , You have clicked the about button ');
  
  }

  sayService($event : any) {
    this.client.emit('Hello service button , You have clicked the Services button ');
  
  }

  sayContact($event : any) {
    this.contact.emit('Hello Contact button , You have clicked the Contact button ');
  
  }



}
